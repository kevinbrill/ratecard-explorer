/**
 * Created by kevin on 8/28/14.
 */

var koa = require('koa');
var json = require('koa-json');
var route = require('koa-route');
var co = require('co');
var sql = require('co-mssql');
var _ = require('lodash');

var app = koa();


var config = {
    user: 'appTerra',
    password: 'sOUL6lkN',
    server: 'easql2k801.turner.com',
    database: 'terra_database',
    pool: {
        max: 10,
        min: 0,
        idleTimeoutMillis: 30000
    },
    options: {
        instanceName: 'researchprd01'
    }
};

app.use(route.get('/xml/:id', getXml));
app.use(route.get('/ratecard/:id', getRatecard));
app.use(route.get('/ratecard', getRatecards));

app.listen(process.env.PORT || 3000);


function *getRatecardById(id) {

    var connection = new sql.Connection(config);

    try {
        yield connection.connect();

        var request = new sql.Request(connection);

        request.input('id', sql.Int, id);

        return yield request.query("SELECT * FROM tRatecardExport WHERE RateCardExportID=@id");

    } catch(ex) {
        console.log(ex);
    }
}

function *getRatecardExports() {

    var connection = new sql.Connection(config);

    try {
        yield connection.connect();

        var request = new sql.Request(connection);

        return yield request.query("SELECT RatecardExportID, UserID, ProjectID, NetworkID, ExportDate, RatecardExportStatusID, Description, TSVFactor FROM tRatecardExport");

    } catch(ex) {
        console.log(ex);
    }
}

function *getRatecard(id) {

    var recordset = yield getRatecardById(id);

    this.body = (recordset[0]) ? recordset[0] : null;
    this.body.hasXml = !_.isUndefined(this.body.RatecardXML) && (this.body.RatecardXML !== null) && (this.body.RatecardXML !== '');

    delete this.body.RatecardXML;

    this.response.type = 'application/json';
}

function *getRatecards() {

    var ratecardExports = yield getRatecardExports();

    this.response.type = 'application/json';
    this.body = ratecardExports;
}


function *getXml(id) {

    var recordset = yield getRatecardById(id);

    this.response.type = 'text/xml';
    this.body = (recordset[0]) ? recordset[0].RatecardXML : '';
}
